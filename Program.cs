﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Excel = Microsoft.Office.Interop.Excel;

namespace Import_to_Excel {
	class Program {

		///<summary>A Mock Table to Export</summary>
		DataTable DT;

		///<summary>Holds the Missing Value???</summary>
		object misvalue;

		///<summary>An Instance of of Excel</summary> 
		Excel.Application app;
		///<summary>A Workbook</summary>
		Excel.Workbook workbook;
		///<summary>A Worksheet</summary>
		Excel.Worksheet worksheet;
		Excel.Range range;

		///<summary>Initialize a new Excel Workbooks</summary>
		public Program( ) {
			// Start a new Excel Instance
			app = new Excel.Application( );

			// Set Visibility and Alerts
			app.Visible = false;
			app.DisplayAlerts = false;

			// make a new Workbook
			misvalue = Type.Missing;
			workbook = app.Workbooks.Add( misvalue );

			// Work sheet
			worksheet = ( Excel.Worksheet ) workbook.ActiveSheet;
			worksheet.Name = "Test Worksheet";

			// Title the Worksheet
			worksheet.Cells[ 1, 1 ] = "Testing Import to Excel";
			worksheet.Cells[ 1, 2 ] = "Date: " + DateTime.Now.ToShortDateString( );
		}

		///<summary>Create a Mock Table</summary>
		public void CreateDataTable( ) {
			// Create a new Data table
			DT = new DataTable( );

			// Add 4 Columns with its type definition 
			DT.Columns.Add( "Testing 1", typeof(string) );
			DT.Columns.Add( "Testing 2", typeof(string) );
			DT.Columns.Add( "Testing 3", typeof(string) );
			DT.Columns.Add( "Testing 4", typeof(string) );

			// Add a row and define the value of each columns for the row
			DT.Rows.Add( "Chess Club", DateTime.Today.ToShortDateString( ), 25, "Little Food cuz little Members" );
			DT.Rows.Add( "TSA Club", DateTimeOffset.Parse( "12/27/2014" ).Date.ToShortDateString( ), 55, "Big Donation; surplus" );
			DT.Rows.Add( "FBLA Club", DateTimeOffset.Parse( "11/30/2014" ).Date.ToShortDateString( ), 127, "Surplus!" );

			// Make a mini console table
			for ( int x = 0; DT.Columns.Count > x; x++ )
				// Columns name
				Console.Write( DT.Columns[ x ].ToString( ) + " || " );
			Console.WriteLine( );

			// Get each row
			foreach ( DataRow row in DT.Rows ) {
				// Get each object in the row
				for ( int x = 0; DT.Rows.Count >= x; x++ ) {
					Console.Write( row.Field<string>( x ) + " || " );
				}
				Console.WriteLine( );
			}
			Console.Read( );
		}

		///<summary>Export to an Excel File</summary>
		public void ExportTable( ) {
			// Format of Excel:
			// [a,b,c,d,e]
			// [f,g,h,i,j]
			// [k,l,m,n,o]
			// 
			// [row, column]
			// [1, 1] = "a"
			// [2, 1] = "f"
			// [3, 1] = "c"
			// [3, 3] = "m"

			// Start making the table
			// Start at the first column (1) and increase by one for each columns in the Data Table
			for ( int x = 1; DT.Columns.Count > x - 1; x++ ) {
				// Label the Cell
				worksheet.Cells[ 2, x ] = DT.Columns[ x - 1 ].ToString( );

				// Get each row
				for ( int y = 3; DT.Rows.Count > y - 3; y++ )
					worksheet.Cells[ y, x ] = DT.Rows[ y - 3 ].Field<string>( x - 1 );
			}

			try {
				// Save to My Documents
				workbook.SaveAs( "Test Worksheet.xls", Excel.XlFileFormat.xlWorkbookNormal, misvalue, misvalue, misvalue, misvalue, Excel.XlSaveAsAccessMode.xlExclusive, misvalue, misvalue, misvalue, misvalue, misvalue );
				workbook.Close( true, misvalue, misvalue );
				app.Quit( );

				// Clear memory
				System.Runtime.InteropServices.Marshal.ReleaseComObject( worksheet );
				System.Runtime.InteropServices.Marshal.ReleaseComObject( workbook );
				System.Runtime.InteropServices.Marshal.ReleaseComObject( app );
			} catch(Exception e ) {
				Console.ForegroundColor = ConsoleColor.Red;
				Console.WriteLine( e );
			} finally {
				GC.Collect( );
			}

			Console.WriteLine( "Finished" );
			Console.ReadLine( );
		}

		[STAThread]
		static void Main( string[ ] args ) {
			Program p = new Program( );
			p.CreateDataTable( );
			p.ExportTable( );
		}
	}
}